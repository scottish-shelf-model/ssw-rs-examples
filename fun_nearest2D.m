function [PointID Distance] = fun_nearest2D(xloc, yloc, x, y)
%
% Find the nearest point in a 2D plane to a given location
%
% USAGE: [PointID Distance] = fun_nearest2D(xloc, yloc, x, y)
%
% xloc = x coordinate of location to find
% yloc = y coordinate of location to find
% x    = x coordinates of all locations within 2D field
% y    = y coordinates of all locations within 2D field
%
% Rory O'Hara Murray
%

for ii=1:length(xloc)
    radvec = sqrt( (xloc(ii)-x).^2 + (yloc(ii)-y).^2);
    [Distance(ii),PointID(ii)] = min(radvec);
end

return