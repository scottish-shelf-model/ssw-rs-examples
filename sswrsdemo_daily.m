%% Demonstration of how to read in daily data form SSW-RS into Matlab

%% 1. Define filename and display information about the netcdf structure
clear, close all

% define path and filename to work with
daily_path = 'sswrs_data/Daily/';
filename = [daily_path 'SSWRS_V1_NOC_FVCOM_NWEuropeanShelf_01dy_19930610-1200_RE.nc'];

% examine structure of a daily average file
% each file contains 10 daily mean values
ncdisp(filename);

%% 2.  Read in the mesh structure and time array
M.time = ncread(filename,'time') ;
% M.Times = ncread(filename, 'Times');
% M.Itime2 = ncread(filename, 'Itime2');
% M.Itime = ncread(filename, 'Itime');
M.nv = ncread(filename,'nv') ;
M.lonc = ncread(filename,'lonc') ;
M.latc = ncread(filename,'latc') ;
M.lon = ncread(filename,'lon') ;
M.lat = ncread(filename,'lat') ;
M.h = ncread(filename,'h') ;

%Adjust longitude scale from 0 to 360, to, -180 to 180 : 
M.lon(M.lon>180)= M.lon(M.lon>180)-360;
M.lonc(M.lonc>180)= M.lonc(M.lonc>180)-360;

% FVCOM reference time is Modified Julian time 1858 -11 -17 00:00:00
% Convert time to 'matlab time' 
M.time_matlab = double(M.time) + datenum(1858,11,17);

%% 3. Read in some variables
M.temperature = ncread(filename, 'temp');

%% 4. Examine the bathymetry and the domain
plot_fvcom_field(M, M.h, 'pll');

%% Change the color range etc.
caxis([0 250])
colormap(parula(16))
c = colorbar;

%% Add labels and zoom into the west coast
set(get(c, 'ylabel'), 'string', 'Depth (m)')
title('Scottish Shelf Model - Bathymetry')
axis([-8 -5 56 58])


%% 5. Plot the sea surface temperature for day 1 in the file
plot_fvcom_field(M, M.temperature(:,1,1), 'pll');
axis([-8 -5 56 58.5])
caxis([8 15])

%% 6. Plot both sea surface and bottom temperature
figTemp = figure('position', [675   669   981   413]);
plot_fvcom_field(M, M.temperature(:,1,1), 'pll', 'cli', [6 16], 'axi', [-8 -5 56 58.5], 'fid', subplot(121), 'tit', 'June sea surface temperature (^oC)');
plot_fvcom_field(M, M.temperature(:,end,1), 'pll', 'cli', [6 16], 'axi', [-8 -5 56 58.5], 'fid', subplot(122), 'tit', 'June sea bottom temperature (^oC)');
colormap(parula(16))


