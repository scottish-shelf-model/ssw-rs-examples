%% 1. Define filename and display information about the netcdf structure
clear, close all

% define path and filename to work with
hourly_path = 'sswrs_data/Hourly/';
filename = [hourly_path 'SSWRS_V1_NOC_FVCOM_NWEuropeanShelf_01hr_19930201-0000_RE.nc'];

% examine structure of a daily average file
% each file contains 10 daily mean values
% daily_info = ncinfo([daily_path filename_daily]);
ncdisp(filename);

%% 2.  Read in the mesh structure and time array
M.time = ncread(filename,'time') ;
M.nv = ncread(filename,'nv') ;
M.lonc = ncread(filename,'lonc') ;
M.latc = ncread(filename,'latc') ;
M.lon = ncread(filename,'lon') ;
M.lat = ncread(filename,'lat') ;
M.h = ncread(filename,'h') ;

%Adjust longitude scale from 0 to 360, to, -180 to 180 : 
M.lon(M.lon>180)= M.lon(M.lon>180)-360;
M.lonc(M.lonc>180)= M.lonc(M.lonc>180)-360;

% FVCOM reference time is Modified Julian time 1858 -11 -17 00:00:00
% Convert time to 'matlab time' 
M.time_matlab = double(M.time) + datenum(1858,11,17) ;

%% 3. Read in some variables
M.zeta = ncread(filename, 'zeta');
M.ua = ncread(filename, 'ua');
M.va = ncread(filename, 'va');

M.sa = sqrt(M.ua.^2 + M.va.^2);

%% 4. make an animation of the elevation
plot_fvcom_field(M, M.zeta, 'pll', 'cli', [-2 2])


%% 5. make animation of the depth mean water speeds around Orkney
plot_fvcom_field(M, M.sa, 'pll', 'axi', [-3.6 -2.2 58.5 59.5]);


%% 6. Examine time series at a single point in the Pentland Firth

lon = -3.08;
lat = 58.71;

In = fun_nearest2D(lon, lat, M.lon, M.lat);
Ie = fun_nearest2D(lon, lat, M.lonc, M.latc);

figure;
subplot(211)
plot(M.time_matlab, M.zeta(In, :));
subplot(212)
plot(M.time_matlab, M.ua(Ie, :));


%% 7. Read in data from a single point from more than one file
%  Often erading in the whole 2D/3D field for a long time period can be
%  slow and you may run low on RAM. So, just reading in the data you need
%  is a good idea.

num_nodes = 267744;
num_elems = 521193;
num_sigma = 20;
num_times_per_file = 24;
num_files = 3;

% define filenames
filenames = {'SSWRS_V1_NOC_FVCOM_NWEuropeanShelf_01hr_19930201-0000_RE.nc', 'SSWRS_V1_NOC_FVCOM_NWEuropeanShelf_01hr_19930202-0000_RE.nc', 'SSWRS_V1_NOC_FVCOM_NWEuropeanShelf_01hr_19930203-0000_RE.nc'};

% declare variables
M2.time = NaN*ones(num_times_per_file*num_files,1);
M2.zeta = NaN*ones(num_times_per_file*num_files,1);
M2.ua   = NaN*ones(num_times_per_file*num_files,1);
M2.va   = NaN*ones(num_times_per_file*num_files,1);

% loop through each file in turn
for ff=1:num_files
    filename_tmp = [hourly_path, filenames{ff}];
    tt = [1:num_times_per_file] + (ff-1)*num_times_per_file;
    M2.time(tt) = ncread(filename_tmp, 'time');
    M2.zeta(tt) = ncread(filename_tmp, 'zeta', [In, 1], [1, Inf]);
    M2.ua(tt)   = ncread(filename_tmp, 'ua', [Ie, 1], [1, Inf]);
    M2.va(tt)   = ncread(filename_tmp, 'va', [Ie, 1], [1, Inf]);
end

% Convert time to 'matlab time' (FVCOM reference time is Modified Julian time 1858 -11 -17 00:00:00)
M2.time_matlab = double(M2.time) + datenum(1858,11,17) ;

%% 8. plot 2 day time series
figure

subplot(311)
plot(M2.time_matlab, M2.zeta);
set(gca, 'xticklabels', datestr(get(gca, 'xtick'), 'dd-mmm'))
ylabel('zeta (m)')

subplot(312)
plot(M2.time_matlab, M2.ua);
set(gca, 'xticklabels', datestr(get(gca, 'xtick'), 'dd-mmm'))
ylabel('ua (m/s)')

subplot(313)
plot(M2.time_matlab, M2.va);
set(gca, 'xticklabels', datestr(get(gca, 'xtick'), 'dd-mmm'))
ylabel('va (m/s)')